datalife_stock_price_list
=========================

The stock_price_list module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-stock_price_list/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-stock_price_list)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
